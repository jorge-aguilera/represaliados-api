use std::{fs::File,io::{BufRead, BufReader},};
use rand::Rng;
use futures::StreamExt;
use std::convert::Infallible;
use std::time::Duration;
use tokio::time::interval;
use warp::{sse::ServerSentEvent, Filter};
use serde::{Serialize};
use positioned_io_preview::{ReadAt, ReadBytesAtExt};

const FILENAME : &'static str = "represaliados.txt";

fn pick_represalido() -> String{
    let file = BufReader::new(File::open(FILENAME).expect("file not found:"));

    let mut rng = rand::thread_rng();
    let pos = rng.gen_range(0,1_000_000);
    let mut idx = 0;
    for line in file.lines(){
        if idx == pos {
            return line.unwrap()
        }
        idx += 1;
    }

    String::new()
}

// create server-sent event
fn sse_counter(item: String) -> Result<impl ServerSentEvent, Infallible> {
    Ok(warp::sse::data(item))
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let sse = warp::path("ticks").and(warp::get()).map(|| {
        // create server event source
        let event_stream = interval(Duration::from_secs(5)).map(move |_| {
            let item = pick_represalido();
            sse_counter(item)
        });
        // reply using server-sent events
        warp::sse::reply(event_stream)
    });

    let www = warp::path("static")
        .and(warp::fs::dir("www"));

    let routes = warp::get().and(sse).or(www);

    warp::serve(routes).run(([0, 0, 0, 0], 3030)).await;
}
